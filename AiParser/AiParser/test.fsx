﻿let __inline cmd = ()
let __isInt x = __inline "ATOM"; true
let __break = __inline "BRK"
let __print x = __inline "DBUG"
let __push x = ()

let length lst =
    let mutable res = 0
    let mutable l = lst
    while not (__isInt l) do
        l <- List.tail l
        res <- res + 1
    res

let doStep acc world =
    let map = fst world    
    let height = length map
    let width = length (List.head map)
    (*let mutable rowsBelow = map
    __print map
    let mutable r = 0    
    while r < height do
        let mutable i = 256 - width
        while i > 0 do
            __inline "LDC 0"
            i <- i - 1
        let mutable row = fst rowsBelow
        let mutable c = 0
        while c < width do
            __push (fst row)
            row <- snd row        
            c <- c + 1
            __print c
        __print r
        __inline "LDF $dumpMap_inner"
        __print r
        __inline "TAP 256"
        __inline "$dumpMap_inner"     
        __print map  
        __print rowsBelow
        rowsBelow <- snd rowsBelow
        __print 125
        __print r
        r <- r + 1*)
    let pacmanStatus = fst (snd world)
    let ghostsStatus = fst (snd (snd world))
    __print ghostsStatus
    let fruitStatus  = snd (snd (snd world))
    let mutable newAcc = 16525 * acc
    let c = 1014223
    newAcc <- newAcc + c
    __inline "BRK"
    let x = if acc + 5 <> newAcc - 19 then newAcc else newAcc
    __break
    (x, newAcc % 4)

let step acc world =
    doStep acc world

let main world ghosts =
    (1, step)
