﻿open System
open System.IO
open Microsoft.FSharp.Compiler.SourceCodeServices
open Microsoft.FSharp.Compiler.Ast
open Nessos.UnionArgParser

let getTree file =
    let input = File.ReadAllText(file)
    let checker = InteractiveChecker.Create()
    let projOptions = checker.GetProjectOptionsFromScript(file, input) |> Async.RunSynchronously
    let parseFileResults = checker.ParseFileInProject(file, input, projOptions) |> Async.RunSynchronously
    match parseFileResults.ParseTree with
    | Some tree -> tree
    | None -> sprintf "Error: %A" parseFileResults.Errors |> failwith

type MapUtils =
    static member union (p:Map<'a,'b>) (q:Map<'a,'b>) = Map(Seq.concat [ (Map.toSeq p) ; (Map.toSeq q) ])
type Convert =    
    static member identToString (p: SynPat) =
        match p with 
        | SynPat.Paren(sp,_) -> Convert.identToString sp
        | SynPat.Typed(sp,_,_) -> Convert.identToString sp
        | SynPat.LongIdent(id, _, _, _, _, _) -> Convert.identToString id
        | SynPat.Named(_, id, _, _, _) -> Convert.identToString id
        | d -> failwith (sprintf "%A" d)
    static member identToString (LongIdentWithDots(ident, _)) = ident |> List.map Convert.identToString |> String.concat "."
    static member identToString (i: Ident) = i.idText

let currentEnvironment = 0
let bind x cell = Map.ofSeq [(x, cell)]

let rec bindPattern (cell, pat) = match pat with
    | SynPat.Paren(sp, _) -> bindPattern (cell, sp)
    | SynPat.Typed(sp,_,_) -> bindPattern (cell, sp)
    | SynPat.Named(SynPat.Wild(_), ident, _, _, _) -> bind (Convert.identToString ident) cell
    | SynPat.LongIdent(id, _, _, _, _, _) -> bind (Convert.identToString id) cell
    | _ -> Map.empty

let nameOfDecl = function
| (SynModuleDecl.Let(_, [Binding(_, _, _, _, _, _, _, pat, _, _, _, _)], _)) -> Convert.identToString pat
| d -> failwith (sprintf "%A" d)

let findDeclaration name = List.find (fun d -> nameOfDecl d = name)
let numbersFrom start = Seq.unfold (fun n -> Some(n, n+1)) start
let ensureFunName (s: string) = if s.StartsWith("$") then s else "$" + s
let addressFun x = sprintf "LDF %s" (ensureFunName x)
let emitFun name body = [ensureFunName name] @ body
let isInline (op: string) = op.StartsWith "__" && op <> "__inline"

let loadFromEnv (vars: Map<string, int>) env x = sprintf "LD %d %d" env (vars.[x])
let storeToEnv env cell = sprintf "ST %d %d" env cell

type Fun = Fun of string * (*local*) int * (*arity*) int * string list option
type FunInfo = Direct of Fun | Inline of SynExpr
let applyFun (Fun(name, local, arity, _)) = (List.replicate local "LDC 0") @ [addressFun name; sprintf "AP %d" (local+arity)]

type BinaryOrder = Left2Right | Right2Left
let binaryOps = 
    [("op_Addition", ("ADD", BinaryOrder.Left2Right)); 
     ("op_Multiply", ("MUL", BinaryOrder.Left2Right));      
     ("op_Subtraction", ("SUB", BinaryOrder.Left2Right)); 
     ("op_Division", ("DIV", BinaryOrder.Left2Right));
     ("op_GreaterThan", ("CGT", BinaryOrder.Left2Right));
     ("op_LessThan", ("CGT", BinaryOrder.Right2Left));
     ("op_GreaterThanOrEqual", ("CGTE", BinaryOrder.Left2Right));
     ("op_LessThanOrEqual", ("CGTE", BinaryOrder.Right2Left));
     ("op_Equality", ("CEQ", BinaryOrder.Left2Right));
     ("op_BooleanAnd", ("MUL", BinaryOrder.Left2Right)); 
     ("op_BooleanOr", ("ADD", BinaryOrder.Left2Right));
    ]
    |> Map.ofList

let unaryOps = 
    [("fst", "CAR"); 
     ("List.head", "CAR"); 
     ("snd", "CDR"); 
     ("List.tail", "CDR"); 
    ]
    |> Map.ofList

module Stdlib =
    let private asmMod = ["LD 0 0"; "LD 0 0"; "LD 0 1"; "DIV"; "LD 0 1"; "MUL"; "SUB"; "RTN"]

    let functions = [("mod", Fun("mod", 0, 2, Some asmMod))] |> Map.ofList

let rec stripTypedPat = function
    | SynPat.Paren(sp, _) -> stripTypedPat sp
    | SynPat.Typed(sp, _, _) -> stripTypedPat sp
    | sp -> sp

let rec stripTypedExpr = function
    | SynExpr.Paren(sp, _, _, _) -> stripTypedExpr sp
    | SynExpr.Typed(sp, _, _) -> stripTypedExpr sp
    | sp -> sp

let rec flattenApps funcs = function
    | SynExpr.Paren(se, _, _, _) -> flattenApps funcs se
    | SynExpr.Typed(se,_,_) -> flattenApps funcs se
    | SynExpr.App(_, false, SynExpr.Ident f, arg0, _) when Map.containsKey f.idText funcs -> 
        Some (funcs.[f.idText], [arg0])
    | SynExpr.App(_, false, curried, arg, _) -> 
        match (flattenApps funcs curried) with
        | Some (f, args) -> Some (f, args @ [arg])
        | _ -> None
    | _ -> None

let rec flattenSequentials = function
    | SynExpr.Paren(se, _, _, _) -> flattenSequentials se
    | SynExpr.Typed(se, _, _) -> flattenSequentials se
    | SynExpr.Sequential(_, _, e1, (SynExpr.Sequential(_, _, _, _, _) as e2), _) -> e1 :: flattenSequentials e2
    | SynExpr.Sequential(_, _, e1, e2, _) -> [e1; e2]
    | _ -> failwith "Illegal content of sequence"

// Given: functions: Map<string, Fun> (global functions), vars: Map<string, int> (cell #s of variables in the local env), AST
// Returns: string list (macro-asm), Map<string, int> (new env state)
let rec exprToAsm funcs vars = function
    | SynExpr.Typed(e, _, _) -> exprToAsm funcs vars e
    | SynExpr.Paren(se, _, _, _) -> exprToAsm funcs vars se
    | SynExpr.Const(SynConst.Unit, _) -> [], vars
    | SynExpr.Null _ -> ["LDC 0"], vars
    | SynExpr.Const(SynConst.Bool _, _) -> [], vars
    | SynExpr.Const(SynConst.Int32 x, _) -> [sprintf "LDC %d" x], vars
    | SynExpr.Ident i when Map.containsKey i.idText vars -> [loadFromEnv vars currentEnvironment i.idText], vars
    | SynExpr.Ident i when Map.containsKey i.idText funcs -> 
        match funcs.[i.idText] with
        | Direct _ -> [addressFun i.idText], vars
        | Inline body -> exprToAsm funcs vars body
    | SynExpr.Paren(expr, _, _, _) -> exprToAsm funcs vars expr
    | SynExpr.ArrayOrListOfSeqExpr(_, SynExpr.CompExpr(true, _, seqs, _), _) ->
        let cmds = flattenSequentials seqs |> List.map (fst << exprToAsm funcs vars)
        (List.foldBack (fun cmdCur cmdRight -> cmdCur @ cmdRight @ ["CONS"]) cmds ["LDC 0"]), vars
    | SynExpr.ArrayOrList(_, elems, _) ->
        let cmds = elems |> List.map (fst << exprToAsm funcs vars)
        (List.foldBack (fun cmdCur cmdRight -> cmdCur @ cmdRight @ ["CONS"]) cmds ["LDC 0"]), vars
    | SynExpr.Tuple(elems, _, _) -> 
        elems |> List.map (fst << exprToAsm funcs vars) |> List.reduceBack (fun cmdCur cmdRight -> cmdCur @ cmdRight @ ["CONS"]), vars
    | SynExpr.App(_, false, SynExpr.LongIdent(_, id, _, _), expr, _) when Map.containsKey (Convert.identToString id) unaryOps ->
        let name = Convert.identToString id
        fst (exprToAsm funcs vars expr) @ [unaryOps.[name]], vars
    | SynExpr.App(_, false, SynExpr.Ident id, expr, _) when Map.containsKey (Convert.identToString id) unaryOps ->
        let name = Convert.identToString id
        fst (exprToAsm funcs vars expr) @ [unaryOps.[name]], vars
    | SynExpr.App(_, true, SynExpr.Ident op, SynExpr.Tuple([left; right], _, _), _) when op.idText = "op_ColonColon" ->
        fst (exprToAsm funcs vars left) @ fst (exprToAsm funcs vars right) @ ["CONS"], vars
    | SynExpr.App(_, false, SynExpr.App(_, true, SynExpr.Ident op, left, _), right, _) when Map.containsKey op.idText binaryOps ->
        let cmd, order = binaryOps.[op.idText]
        match order with
        | BinaryOrder.Left2Right -> fst (exprToAsm funcs vars left) @ fst (exprToAsm funcs vars right) @ [cmd], vars
        | BinaryOrder.Right2Left -> fst (exprToAsm funcs vars right) @ fst (exprToAsm funcs vars left) @ [cmd], vars
    | SynExpr.App(_, false, SynExpr.Ident op, expr, _) when op.idText = "op_LogicalNot" || op.idText = "not" ->
        // x = 0
        let res, _ = exprToAsm funcs vars expr
        ["LDC 0"] @ res @ ["CEQ"], vars
    | SynExpr.App(_, false, SynExpr.App(_, true, SynExpr.Ident op, left, _), right, _) when op.idText = "op_Inequality" ->
        // (x = y) = 0
        let cmd, _ = binaryOps.["op_Equality"]
        let res = fst (exprToAsm funcs vars left) @ fst (exprToAsm funcs vars right) @ [cmd]
        ["LDC 0"] @ res @ ["CEQ"], vars
    | SynExpr.App(_, false, SynExpr.App(_, true, SynExpr.Ident op, left, _), right, _) when op.idText = "op_Modulus" ->    
        fst (exprToAsm funcs vars left) @ fst (exprToAsm funcs vars right) @ (Stdlib.functions.["mod"] |> applyFun), vars
    | SynExpr.App(_, false, SynExpr.Ident op, arg, _) when Map.containsKey op.idText funcs ->
        let res, vars' = exprToAsm funcs vars arg
        match funcs.[op.idText] with
        | Direct f -> res @ applyFun f, vars'
        | Inline inl -> 
            let res', vars'' = exprToAsm funcs vars' inl
            res @ res', vars''
    | SynExpr.App(_, false, SynExpr.Ident op, SynExpr.Const(SynConst.String(cmd, _), _), _) when op.idText = "__inline" ->
        [cmd], vars
    | SynExpr.App(_, false, _, _, _) as app ->
        match (flattenApps funcs app) with
        | Some (Direct f, args) -> (args |> List.collect (fst << exprToAsm funcs vars)) @ (applyFun f), vars
        | Some (Inline inl, args) -> 
            let resArgs = args |> List.collect (fst << exprToAsm funcs vars)
            let res, vars' = exprToAsm funcs vars inl
            resArgs @ res, vars'
        | None -> failwith (sprintf "Not supported:\n%A" app)    
    | SynExpr.Sequential(_, _, e1, e2, _) -> 
        let res1, vars1 = exprToAsm funcs vars e1
        let res2, vars2 = exprToAsm funcs vars1 e2
        res1 @ res2, vars2
    | SynExpr.LongIdentSet(id, value, _) -> 
        let res, vars' = exprToAsm funcs vars value
        let cell = vars.[Convert.identToString id]
        res @ [storeToEnv currentEnvironment cell], vars'
    | SynExpr.IfThenElse(cond, eThen, eElseOpt, _, _, _, _) ->
        let condRes, _ = exprToAsm funcs vars cond
        let thenRes, _ = exprToAsm funcs vars eThen
        let elseRes, _ = match eElseOpt with
            | Some eElse -> exprToAsm funcs vars eElse
            | None -> [], Map.empty
        condRes @ ["%IF"] @ thenRes @ ["%ELSE"] @ elseRes @ ["%ENDIF"], vars
    | SynExpr.While(_, cond, body, r) ->
        let label = sprintf "$while_%d_%d" r.StartLine r.StartColumn
        let resCond, _ = exprToAsm funcs vars cond
        let resBody, _ = exprToAsm funcs vars body
        [label] @ resCond @ ["%IF"] @ resBody @ [sprintf "%%JMP %s" label; "%ELSE"; "%ENDIF"], vars
    | SynExpr.LetOrUse(_, _, [Binding(_, _, _, _, _, _, _, pat, _, init, _, _)], body, _) as d ->
        let cell = vars.Count
        let vars' = bindPattern (cell, pat) |> MapUtils.union vars
        let res, varsRes = exprToAsm funcs vars' body
        fst (exprToAsm funcs vars init) @ [storeToEnv currentEnvironment cell] @ res, varsRes
    | e -> failwith (sprintf "Not supported:\n%A" e)

let fun2asm funcs = function    
    // We only support one binding per let (which means 'let .. and ... ' are unsupported for now).
    | SynModuleDecl.Let(_, [Binding(_, _, _, _, _, _, _, pat, _, body, _, _)], _) -> 
        let name = Convert.identToString pat
        let (SynPat.LongIdent(LongIdentWithDots(_, _), _, _, (SynConstructorArgs.Pats args), _, _)) = pat
        let vars = Seq.zip (numbersFrom 0) args |> Seq.map bindPattern |> Seq.reduce MapUtils.union
        emitFun name (exprToAsm funcs vars body |> fst) @ ["RTN"; ensureFunName "end_" + name]
    | e -> failwith (sprintf "Not supported:\n%A" e)

let arity = function
    | SynModuleDecl.Let(_, [Binding(_, _, _, _, _, _, _, SynPat.LongIdent(_, _, _, SynConstructorArgs.Pats args, _, _), _, body, _, _)], _) -> args.Length
    | SynModuleDecl.Let(_, [Binding(_, _, _, _, _, _, _, _, _, body, _, _)], _) -> 0
    | d -> failwith (sprintf "%A" d)

let buildFuns decls =
    let names = 
        decls 
        |> List.choose (function 
                        | (SynModuleDecl.Let(_, [Binding(_, _, _, _, _, _, _, _, _, body, _, _)], _) as d) -> Some (nameOfDecl d, body) 
                        | _ -> None)
        |> Map.ofList
    let rec variableCount = function
    | SynExpr.Paren(se, _, _, _) -> variableCount se
    | SynExpr.Typed(se, _, _) -> variableCount se
    | SynExpr.App(_, false, SynExpr.Ident op, arg, _) when isInline op.idText -> 
        variableCount arg + variableCount names.[op.idText]
    | SynExpr.App(_, false, SynExpr.Ident op, arg, _) when Map.containsKey op.idText names -> variableCount arg
    | SynExpr.App(_, false, SynExpr.Ident op, arg, _) -> variableCount arg
    | SynExpr.Sequential(_, _, e1, e2, _) -> variableCount e1 + variableCount e2
    | SynExpr.IfThenElse(cond, eThen, Some eElse, _, _, _, _) -> variableCount cond + variableCount eThen + variableCount eElse
    | SynExpr.IfThenElse(cond, eThen, None, _, _, _, _) -> variableCount cond + variableCount eThen
    | SynExpr.While(_, _, body, _) -> variableCount body
    | SynExpr.LetOrUse(_, _, [Binding(_, _, _, _, _, _, _, _, _, init, _, _)], body, _) -> 
        1 + variableCount init + variableCount body
    | _ -> 0
    let makeFun (d: SynModuleDecl) =
        let (SynModuleDecl.Let(_, [Binding(_, _, _, _, _, _, _, _, _, body, _, _)], _)) = d
        let name = nameOfDecl d
        let info = if isInline name then Inline body else Direct (Fun(nameOfDecl d, variableCount body, arity d, None))
        (name, info)
    decls |> List.map makeFun |> Map.ofList

let file2asm = function
    | ParsedInput.ImplFile(ParsedImplFileInput(_, _, _, _, _, [SynModuleOrNamespace(_, _, decls, _, _, _, _)], _)) ->        
        let nonspecial = decls |> List.filter (fun d -> nameOfDecl d <> "__inline")
        let funcs = buildFuns nonspecial
        let mainFun = findDeclaration "main" decls        
        let others = decls |> List.filter (fun f -> let name = nameOfDecl f in name <> "main" && not (name.StartsWith "__"))
        let stdlib = Stdlib.functions 
                     |> Seq.collect (fun kvp -> let (Fun(name, _, _, Some code)) = kvp.Value in emitFun name code)
                     |> Seq.toList
        (fun2asm funcs mainFun) @ (List.collect (fun2asm funcs) others) @ stdlib

type Arguments =
    | [<Mandatory>] [<First>] File of string
    | Ast
with interface IArgParserTemplate with
        member s.Usage = 
            match s with
            | File _ -> "the F# script to translate"
            | Ast -> "print F# AST instead of macro-asm"

[<EntryPoint>]
let main argv =
    let parser = UnionArgParser<Arguments>()
    let arguments = parser.Parse argv
    let tree = arguments.GetResult <@ File @> |> Path.GetFullPath |> getTree
    if arguments.Contains <@ Ast @> then
        tree |> printfn "%A"
    else
        file2asm tree |> List.iter (printfn "%s")
    0
