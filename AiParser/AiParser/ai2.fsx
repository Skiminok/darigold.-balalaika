﻿let __inline cmd = ()
let __isInt x = __inline "ATOM"; true
let __break = __inline "BRK"
let __print x = __inline "DBUG"
let __push x = ()

////////////////// STDLIB.LIST

let testSingleton (x: int) : int list =
    (x+1) :: []

let getArgmaxAndMax lst =
    let mutable max = List.head lst
    let mutable argmax = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur > max then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getElement list index =
    let mutable list_pointer = list
    let mutable list_index = 0
    while list_index <> index do
        list_pointer <- List.tail list_pointer
        list_index <- list_index + 1
    List.head list_pointer

let getLength lst =
    let mutable res = 0
    let mutable l = lst
    while not (__isInt l) do
        l <- List.tail l
        res <- res + 1
    res

let getMapDimensions map =
    let height = getLength map
    let width = getLength (List.head map)
    (height, width)

let getMatrixElementImpl matrix coordinates =
    let c = fst coordinates
    let r = snd coordinates
    let row = getElement matrix r
    let element = getElement row c
    element

let getMatrixElement matrix dims coordinates =
    let height = fst dims
    let width = snd dims
    let c = fst coordinates
    let r = snd coordinates
    let element = if c < 0 || c >= width || r < 0 || r >= height then -1 else getMatrixElementImpl matrix coordinates
    element

////////////////// STDLIB.MATH

let abs x =
    if x > 0 then x else 0 - x

let dist pos1 pos2 = 
    abs (fst pos1 - fst pos2) + abs (snd pos1 - snd pos2)

let thresh x t = 
    if x > t then t else x

let thresh_default x t d =
    if x > t then d else x

let get_random_move acc = 
    let newAcc = (16525 * acc + 1014223) % 1296871
    (newAcc, newAcc % 4)

////////////////// STDLIB.MAP

let is_valid_move mv map dims =
    (getMatrixElement map dims mv) > 0

let move cur_pos dir = 
    let c = fst cur_pos
    let r = snd cur_pos
    let mutable result = (c-1, r) // LEFT
    if dir = 0 then // UP
        result <- (c, r-1)
    if dir = 1 then // RIGHT
        result <- (c+1, r)
    if dir = 2 then // DOWN
        result <- (c, r+1)
    result
   
////////////////// OUR STRATEGY

// our strategy: look for at all possible moves, maximize total distance to ghosts
let compute_distance_to_ghosts our_pos ghost_states =
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 0
    while not (__isInt ghost_pointer) do
        let ghost_pos = fst (snd (List.head ghost_pointer))
        let dist = dist our_pos ghost_pos
        cur_distance <- cur_distance + (thresh dist 9)
        ghost_pointer <- List.tail ghost_pointer
    cur_distance

let thresholded_distance map dims ghost_states proposed_move =
    if is_valid_move proposed_move map dims then compute_distance_to_ghosts proposed_move ghost_states else 0

let find_move map dims cur_pos ghost_states =
    let dist0 = thresholded_distance map dims ghost_states (move cur_pos 0)
    let dist1 = thresholded_distance map dims ghost_states (move cur_pos 1)
    let dist2 = thresholded_distance map dims ghost_states (move cur_pos 2)
    let dist3 = thresholded_distance map dims ghost_states (move cur_pos 3)    
    let amm = getArgmaxAndMax [dist0; dist1; dist2; dist3]
    let result = fst amm    
    result

let doStep acc world =
    let map = fst world
    let dims = getMapDimensions map
    let pacmanStatus = fst (snd world)
    let ghostsStatus = fst (snd (snd world))
    let pacmanPosition = fst (snd pacmanStatus)
    let new_move = find_move map dims pacmanPosition ghostsStatus
    (acc, new_move)

let step acc world =    
    doStep acc world

let main world ghosts =
    (1, step)
