﻿let __dumpMap map = ()
let __inline cmd = ()
let __isInt x = true
let __break = ()
let __print x = printfn "%A" x

let doStep acc world =
    let map = fst world
    __dumpMap map
    let pacmanStatus = fst (snd world)
    let ghostsStatus = fst (snd (snd world))
    __print ghostsStatus
    let fruitStatus  = snd (snd (snd world))
    let mutable newAcc = 16525 * acc
    let c = 1014223
    newAcc <- newAcc + c
    __inline "BRK" 
    let x = if acc + 5 <> newAcc - 19 then newAcc else newAcc
    __break
    (x, newAcc % 4)

let step acc world =    
    doStep acc world

let main world ghosts =
    (1, step)
