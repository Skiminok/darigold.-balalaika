#!/usr/bin/python3
import sys
input_lines = map(str.strip, sys.stdin)

TMP_POS = 64

def process_labels(lines):
    labels = {}
    offset = 0
    for i, line in enumerate(lines):
      if line[0] == "$":

        labels[line.split()[0]] = str(i - offset)
        lines[i] = " ".join(line.split()[1:])
        if len(lines[i]) == 0:
            offset += 1


    for i, line in enumerate(lines):
      tokens = line.split()
      tokens = map(lambda x: "" if x == "" else labels[x] if x[0] == "$" else x, tokens)
      lines[i] = " ".join(tokens)
    return lines


def dup(ops):
    return ["ST 0 0",
            "LD 0 0",
            "LD 0 0"]

def mod(ops):
    m = ops[1]
    pass


def jmp(ops):
    label = ops[1]
    return ["LDC 1",
            "TSEL %s %s" % (label, label)]


branch_idx = 0
cur_if_label = ""

if_labels = []

def if_op(ops):
    global branch_idx, cur_if_label, if_labels
    branch_idx += 1
    cur_if_label = "$_if_label_%d" % branch_idx
    then_label = cur_if_label + "_then"
    else_label = cur_if_label + "_else"
    res = ["TSEL %s %s" % (then_label, else_label),
           then_label]
    if_labels.append(cur_if_label)
    return res


def else_op(ops):
    cur_if_label = if_labels[-1]
    #cur_if_label = "$_if_label_%d" % branch_idx
    end_label = cur_if_label + "_end"
    res = jmp([None, end_label])
    else_label = cur_if_label + "_else"
    res.append(else_label)
    return res

def endif_op(ops):
    cur_if_label = if_labels.pop()
    #cur_if_label = "$_if_label_%d" % branch_idx
    end_label = cur_if_label + "_end"
    return [end_label]


def call(ops):
    # %call semantics: %call func_name arg0 arg1 arg2 ...
    # func_name - label of the function
    # arg0 - either interger, or pair of integers (i j) - get value from env i and gets value j
    res = []
    funcname = ops[1]
    num_local = int(ops[2])
    ops = ops[3:]
    for op in ops:
        if op[0] != "(":
            res.append("LDC " + op)
        else:
            op = op[1:-1]
            op = op.replace(",", " ")
            res.append("LD " + op)
    for i in range(num_local):
        res.append("LDC 0")

    res.append("LDF " + funcname)

    res.append("AP %d" % (len(ops) + num_local))
    return res


functions = {
            "%DUP" : dup,
            "%CALL" : call,
            "%JMP" : jmp,
            "%IF" : if_op,
            "%ELSE" : else_op,
            "%ENDIF" : endif_op
            }

def apply_funcs(lines):
    res = []
    for line in lines:
        ops = line.split()
        op = ops[0]
        if op[0] == "$" and len(ops) > 1:
            ops = ops[1:]
            op = ops[0]
        if op in functions:
            res.extend(functions[op](ops))
        else:
            res.append(line)

    return res


def remove_comments(lines):
    for i, line in enumerate(lines):
        j = line.find(';')
        if j >= 0:
            lines[i] = line[:j] 
    return lines

def remove_empty_lines(lines):
    res = list(map(str.strip, lines))
    res = filter(lambda x: len(x) > 0, res)
    return list(res)

transforms = [
                remove_comments,
                remove_empty_lines,
                apply_funcs, 
                remove_empty_lines,
                process_labels,
                remove_empty_lines
             ]


input_lines = list(input_lines)
for f in transforms:
    input_lines = f(input_lines)

print("\n".join(input_lines))
