let __inline cmd = ()
let __isInt x = __inline "ATOM"; true
let __break = __inline "BRK"
let __print x = __inline "DBUG"
let __push x = ()

let getPacmanPosition world =
    let map = fst world
    let pacmanStatus = fst (snd world)
    let position = fst (snd pacmanStatus)
    position

let getElement list index =
    let mutable list_pointer = list
    let mutable list_index = 0
    while list_index <> index do
        list_pointer <- snd list_pointer
        list_index <- list_index + 1
    fst list_pointer

let getMatrixElementImpl matrix coordinates =
    let x = fst coordinates
    let y = snd coordinates
    let row = getElement matrix x
    let element = getElement row y
    element

let getMatrixElement matrix coordinates =
    let MIN = 0
    let MAX = 255
    let x = fst coordinates
    let y = snd coordinates
    let element = if x < MIN || MAX < x || y < MIN || MAX < y then -1 else getMatrixElementImpl matrix coordinates
    element

let doStep acc world =
    let UP = 0
    let RIGHT = 1
    let DOWN = 2
    let LEFT = 3
    let map = fst world
    let pacmanStatus = fst (snd world)
    let ghostsStatus = fst (snd (snd world))
    let fruitStatus = snd (snd (snd world))
    let pacmanPosition = getPacmanPosition world
    let positionToTheLeft = getMatrixElement map (snd pacmanPosition, (fst pacmanPosition) - 1)
    __print pacmanPosition
    __print positionToTheLeft
    let direction = if positionToTheLeft = 0 then UP else LEFT
    (1, direction)

let step acc world =
    doStep acc world

let main world ghosts =
    (1, step)
