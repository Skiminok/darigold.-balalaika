﻿let __inline cmd = ()
let __isInt x = __inline "ATOM"; true
let __break = __inline "BRK"
let __print x = __inline "DBUG"
let __push x = ()

////////////////// STDLIB.LIST

let getArgmaxAndMax lst =
    let mutable max = List.head lst
    let mutable argmax = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur > max then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getArgmaxAndMax_another_iDidntLikeThatOne lst badIndex =
    let mutable max = 0
    let mutable argmax = badIndex
    let mutable i = 0
    let mutable tail = lst
    while not (__isInt tail) do
        let cur = List.head tail
        if (cur > max) || (cur = max && argmax = badIndex) then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getArgminAndMin lst =
    let mutable min = List.head lst
    let mutable argmin = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur < min then
            min <- cur
            argmin <- i
        tail <- List.tail tail
        i <- i + 1
    (argmin, min)

let getElement list index =
    let mutable list_pointer = list
    let mutable list_index = 0
    while list_index <> index do
        list_pointer <- List.tail list_pointer
        list_index <- list_index + 1
    List.head list_pointer

let getLength lst =
    let mutable res = 0
    let mutable l = lst
    while not (__isInt l) do
        l <- List.tail l
        res <- res + 1
    res

let getMapDimensions map =
    let height = getLength map
    let width = getLength (List.head map)
    (height, width)

let getMatrixElementImpl matrix coordinates =
    let c = fst coordinates
    let r = snd coordinates
    let row = getElement matrix r
    let element = getElement row c
    element

let getMatrixElement matrix dims coordinates =
    let height = fst dims
    let width = snd dims
    let c = fst coordinates
    let r = snd coordinates
    let element = if c < 0 || c >= width || r < 0 || r >= height then -1 else getMatrixElementImpl matrix coordinates
    element

let reverse_list list = 
   let mutable list_pointer = list
   let mutable result = []
   while not (__isInt list_pointer) do
        result <- (List.head list_pointer) :: result
        list_pointer <- List.tail list_pointer
   result

////////////////// STDLIB.TREES

let treeSize tree = snd (snd (snd (snd tree)))
let treeRightChild tree = fst (snd (snd (snd tree)))
let treeLeftChild tree = fst (snd (snd tree))
let treeData tree = fst (snd tree)
let treeIsNode tree = fst tree = 1
let treeIsLeaf tree = fst tree = 0

let rec treeKth tree k =
    if treeIsLeaf tree then 0
    else
        let leftChild = treeLeftChild tree
        let sizeLeft = treeSize leftChild
        if sizeLeft = k then treeData tree
        else if sizeLeft > k then treeKth leftChild k
        else treeKth (treeRightChild tree) (k - sizeLeft - 1)

let rec treePreorder tree tail =
    if treeIsLeaf tree then tail
    else 
        let cur = treeData tree
        let leftChild = treeLeftChild tree
        let rightChild = treeRightChild tree
        treePreorder leftChild (cur :: (treePreorder rightChild tail))

let rec array2tree lst level =
    if __isInt lst then
        (0, 0, 0, 0, 0)
    else
        let len = getLength lst
        let mid = len / 2
        let mutable left = []
        let mutable i = 0
        let mutable tail = lst
        while i < mid do
            left <- (List.head tail) :: left
            tail <- List.tail tail
            i <- i + 1
        let elem = List.head tail
        let key = if level = 1 then elem else array2tree elem (level - 1)
        tail <- List.tail tail
        left <- reverse_list left
        let leftTree = array2tree left level
        let rightTree = array2tree tail level
        (1, key, leftTree, rightTree, 1 + (treeSize leftTree) + (treeSize rightTree))

let rec getMapElement maptree coords =
    let c = fst coords
    let r = snd coords
    let row = treeKth maptree r
    treeKth row c

////////////////// STDLIB.MATH

let abs x =
    if x > 0 then x else 0 - x

let dist pos1 pos2 = 
    abs (fst pos1 - fst pos2) + abs (snd pos1 - snd pos2)

let thresh x t = 
    if x > t then t else x

let thresh_default x t d =
    if x > t then d else x

let get_random_move acc = 
    let newAcc = (16525 * acc + 1014223) % 1296871
    (newAcc, newAcc % 4)

////////////////// STDLIB.MAP

let is_valid_move mv map dims =
    (getMatrixElement map dims mv) > 0

let move cur_pos dir = 
    let c = fst cur_pos
    let r = snd cur_pos
    let mutable result = (c-1, r) // LEFT
    if dir = 0 then // UP
        result <- (c, r-1)
    if dir = 1 then // RIGHT
        result <- (c+1, r)
    if dir = 2 then // DOWN
        result <- (c, r+1)
    result

let opposite move =
    if move = 0 then 2 else if move = 1 then 3 else if move = 2 then 0 else 1
   
////////////////// OUR STRATEGY

let getCloseThreshold dims = if (fst dims < snd dims) then (fst dims) / 2 else (snd dims) / 2

// our strategy: look for at all possible moves, maximize total distance to ghosts
let compute_sum_distances_to_ghosts dims our_pos ghost_states =
    let closeThreshold = getCloseThreshold dims
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 0
    while not (__isInt ghost_pointer) do
        let ghost_pos = fst (snd (List.head ghost_pointer))
        let dist = dist our_pos ghost_pos
        cur_distance <- cur_distance + (thresh dist closeThreshold)
        ghost_pointer <- List.tail ghost_pointer
    cur_distance

let compute_min_distance_to_ghosts our_pos ghost_states =
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 999999999
    while not (__isInt ghost_pointer) do
        let ghost_pos = fst (snd (List.head ghost_pointer))
        let dist = dist our_pos ghost_pos
        if dist < cur_distance then
            cur_distance <- dist
        ghost_pointer <- List.tail ghost_pointer
    cur_distance

let thresholded_distance map dims ghost_states proposed_move defolt =
    if is_valid_move proposed_move map dims then 
        compute_sum_distances_to_ghosts dims proposed_move ghost_states 
    else defolt

let find_move map dims cur_pos ghost_states prev_move =
    let dist0 = thresholded_distance map dims ghost_states (move cur_pos 0) 0
    let dist1 = thresholded_distance map dims ghost_states (move cur_pos 1) 0
    let dist2 = thresholded_distance map dims ghost_states (move cur_pos 2) 0
    let dist3 = thresholded_distance map dims ghost_states (move cur_pos 3) 0  
    let amm = getArgmaxAndMax [dist0; dist1; dist2; dist3]
    let careful_result = fst amm
    let farThreshold = (getCloseThreshold dims * 4) / 5
    let mindist = compute_min_distance_to_ghosts cur_pos ghost_states
//    __print (mindist, farThreshold)
    if mindist > farThreshold then  // if we are safe, go eat something
        let content0 = thresh_default (getMatrixElement map dims (move cur_pos 0)) 4 1
        let content1 = thresh_default (getMatrixElement map dims (move cur_pos 1)) 4 1
        let content2 = thresh_default (getMatrixElement map dims (move cur_pos 2)) 4 1
        let content3 = thresh_default (getMatrixElement map dims (move cur_pos 3)) 4 1
        let surroundings = [content0; content1; content2; content3]
//        __print surroundings
        let content_amm = getArgmaxAndMax surroundings
        let min_move = fst content_amm
        if min_move = opposite prev_move then
            let another_amm = getArgmaxAndMax_another_iDidntLikeThatOne surroundings min_move
            fst another_amm
        else min_move
    else
        careful_result

let doStep acc world =
    let prev_move = acc
    let map = fst world
    let dims = getMapDimensions map
    let pacmanStatus = fst (snd world)
    let ghostsStatus = fst (snd (snd world))
    let pacmanPosition = fst (snd pacmanStatus)
    let new_move = find_move map dims pacmanPosition ghostsStatus prev_move
    (new_move, new_move)

let testMap maptree =
    let mutable i = 0
    while i < 22 do
        __print (getMapElement maptree (1, i))
        i <- i + 1

let step acc world =
//    testMap acc    
    doStep acc world

let main world ghosts =       
    (array2tree (fst world) 2, step)
