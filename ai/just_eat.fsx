let __inline cmd = ()
let __isInt x = __inline "ATOM"; true
let __break = __inline "BRK"
let __print x = __inline "DBUG"
let __push x = ()

////////////////// STDLIB.LIST

let getArgmaxAndMax lst =
    let mutable max = List.head lst
    let mutable argmax = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur > max then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getElement list index =
    let mutable list_pointer = list
    let mutable list_index = 0
    while list_index <> index do
        list_pointer <- List.tail list_pointer
        list_index <- list_index + 1
    List.head list_pointer

let getLength lst =
    let mutable res = 0
    let mutable l = lst
    while not (__isInt l) do
        l <- List.tail l
        res <- res + 1
    res

let getMapDimensions map =
    let height = getLength map
    let width = getLength (List.head map)
    (height, width)

let getMatrixElementImpl matrix coordinates =
    let c = fst coordinates
    let r = snd coordinates
    let row = getElement matrix r
    let element = getElement row c
    element

let getMatrixElement matrix dims coordinates =
    let height = fst dims
    let width = snd dims
    let c = fst coordinates
    let r = snd coordinates
    let element = if c < 0 || c >= width || r < 0 || r >= height then -1 else getMatrixElementImpl matrix coordinates
    element

////////////////// STDLIB.MATH

let abs x =
    if x > 0 then x else 0 - x

let dist pos1 pos2 = 
    abs (fst pos1 - fst pos2) + abs (snd pos1 - snd pos2)

let thresh x t = 
    if x > t then t else x

let thresh_default x t d =
    if x > t then d else x

let get_random_move acc = 
    let newAcc = (16525 * acc + 1014223) % 1296871
    (newAcc, newAcc % 4)

////////////////// STDLIB.MAP

let is_valid_move mv map dims =
    (getMatrixElement map dims mv) > 0

let move cur_pos dir = 
    let c = fst cur_pos
    let r = snd cur_pos
    let mutable result = (c-1, r) // LEFT
    if dir = 0 then // UP
        result <- (c, r-1)
    if dir = 1 then // RIGHT
        result <- (c+1, r)
    if dir = 2 then // DOWN
        result <- (c, r+1)
    result

let getPacmanPosition world =
    let map = fst world
    let pacmanStatus = fst (snd world)
    let position = fst (snd pacmanStatus)
    position

let compare_pairs pair_a pair_b =
    fst pair_a = fst pair_b && snd pair_a = snd pair_b

let containsGoodThings position map =
    let matrixDims = getMapDimensions map
    let element = getMatrixElement map matrixDims position
    __print (position, element)
    element >= 2 && element <= 4

let findNearestPill position map =
    let toTheLeft = (fst position, (snd position) - 1)
    if containsGoodThings toTheLeft map then
        3
    else
        let toTheRight = (fst position, (snd position) + 1)
        if containsGoodThings toTheRight map then
            1
        else
            let up = ((fst position) - 1, snd position)
            if containsGoodThings up map then
                0
            else
                let down = ((fst position) + 1, snd position)
                if containsGoodThings down map then
                    2
                else
                    -1


let doStep acc world =
    let UP = 0
    let RIGHT = 1
    let DOWN = 2
    let LEFT = 3
    let map = fst world
    let pacmanStatus = fst (snd world)
    let ghostsStatus = fst (snd (snd world))
    let fruitStatus = snd (snd (snd world))
    let pacmanPosition = getPacmanPosition world
    let seed = fst acc
    let lastDirection = fst (snd acc)
    let previousPacmanPosition = snd (snd acc)
    let new_seed = seed + 273
    let pillDirection = findNearestPill pacmanPosition map
    __print pillDirection
    let mutable direction = lastDirection
    if pillDirection <> -1 then
        direction <- pillDirection
    else
        direction <- (direction + new_seed) % 4
    ((new_seed, direction, pacmanPosition), direction)

let step acc world =
    doStep acc world

let main world ghosts =
    // state: random_seed, previous_direction, previous_position
    ((1, -1, getPacmanPosition world), step)