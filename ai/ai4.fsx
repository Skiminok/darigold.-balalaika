﻿let __inline cmd = ()
let __isInt x = __inline "ATOM"; true
let __break = __inline "BRK"
let __print x = __inline "DBUG"
let __push x = ()
let __false = 0 > 1
let __true = 1 > 0

////////////////// STDLIB.LIST

let getArgmaxAndMax lst =
    let mutable max = List.head lst
    let mutable argmax = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur > max then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getArgmaxAndMax_another_iDidntLikeThatOne lst badIndex =
    let mutable max = 0
    let mutable argmax = badIndex
    let mutable i = 0
    let mutable tail = lst
    while not (__isInt tail) do
        let cur = List.head tail
        if (cur > max) || (cur = max && argmax = badIndex) then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getArgminAndMin lst =
    let mutable min = List.head lst
    let mutable argmin = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur < min then
            min <- cur
            argmin <- i
        tail <- List.tail tail
        i <- i + 1
    (argmin, min)

let getElement list index =
    let mutable list_pointer = list
    let mutable list_index = 0
    while list_index <> index do
        list_pointer <- List.tail list_pointer
        list_index <- list_index + 1
    List.head list_pointer

let getLength lst =
    let mutable res = 0
    let mutable l = lst
    while not (__isInt l) do
        l <- List.tail l
        res <- res + 1
    res

let getMapDimensions map =
    let height = getLength map
    let width = getLength (List.head map)
    (height, width)
    

//let listContains list elem =
//    let mutable i = 0
//    while not (__isInt list) do
//        let cur = List.head list
//        if __isInt elem

////////////////// STDLIB.TREES

let treeSize tree = snd (snd (snd (snd tree)))
let treeRightChild tree = fst (snd (snd (snd tree)))
let treeLeftChild tree = fst (snd (snd tree))
let treeData tree = fst (snd tree)
let treeIsNode tree = fst tree = 1
let treeIsLeaf tree = fst tree = 0

let rec treeKth tree k =
    if treeIsLeaf tree then 0
    else
        let leftChild = treeLeftChild tree
        let sizeLeft = treeSize leftChild
        if sizeLeft = k then treeData tree
        else if sizeLeft > k then treeKth leftChild k
        else treeKth (treeRightChild tree) (k - sizeLeft - 1)

let rec treePreorder tree tail =
    if treeIsLeaf tree then tail
    else 
        let cur = treeData tree
        let leftChild = treeLeftChild tree
        let rightChild = treeRightChild tree
        treePreorder leftChild (cur :: (treePreorder rightChild tail))

let rec array2tree lst level =
    if __isInt lst then
        (0, 0, 0, 0, 0)
    else
        let len = getLength lst
        let mid = len / 2
        let mutable left = []
        let mutable i = 0
        let mutable tail = lst
        while i < mid do
            left <- (List.head tail) :: left
            tail <- List.tail tail
            i <- i + 1
        let elem = List.head tail
        let key = if level = 1 then elem else array2tree elem (level - 1)
        tail <- List.tail tail
        left <- reverse_list left
        let leftTree = array2tree left level
        let rightTree = array2tree tail level
        (1, key, leftTree, rightTree, 1 + (treeSize leftTree) + (treeSize rightTree))

let rec getMapElement maptree coords =
    let c = fst coords
    let r = snd coords
    let row = treeKth maptree r
    treeKth row c

////////////////// STDLIB.MATH

let abs x =
    if x > 0 then x else 0 - x

let dist pos1 pos2 = 
    abs (fst pos1 - fst pos2) + abs (snd pos1 - snd pos2)

let thresh x t = 
    if x > t then t else x

let thresh_default x t d =
    if x > t then d else x

let get_random_move acc = 
    let newAcc = (16525 * acc + 1014223) % 1296871
    (newAcc, newAcc % 4)

////////////////// STDLIB.MAP

let is_valid_move mv maptree =
    (getMapElement maptree mv) > 0

let move cur_pos dir = 
    let c = fst cur_pos
    let r = snd cur_pos
    let mutable result = (c-1, r) // LEFT
    if dir = 0 then // UP
        result <- (c, r-1)
    if dir = 1 then // RIGHT
        result <- (c+1, r)
    if dir = 2 then // DOWN
        result <- (c, r+1)
    result

let opposite move =
    (move + 2) % 4
    // if move = 0 then 2 else if move = 1 then 3 else if move = 2 then 0 else 1
   
////////////////// OUR STRATEGY

let reverse_list list = 
   let mutable list_pointer = list
   let mutable result = []
   while not (__isInt list_pointer) do
        result <- (List.head list_pointer) :: result
        list_pointer <- List.tail list_pointer
   result

let extract_ghost_positions ghost_states =  // return a list of ghost positions [(x1, y1), (x2, y2), ...]
   let mutable ghost_pointer = ghost_states
   let mutable result = []
   while not (__isInt ghost_pointer) do
        let cur_ghost = List.head ghost_pointer
        let ghost_pos = fst (snd cur_ghost)
        result <- ghost_pos :: result

        ghost_pointer <- List.tail ghost_pointer
   result


let getCloseThreshold dims = if (fst dims < snd dims) then (fst dims) / 2 else (snd dims) / 2

// our strategy: look for at all possible moves, maximize total distance to ghosts
let compute_sum_distances_to_ghosts dims our_pos ghost_states =
    let closeThreshold = getCloseThreshold dims
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 0
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let dist = dist our_pos ghost_pos
        cur_distance <- cur_distance + (thresh dist closeThreshold)
        ghost_pointer <- List.tail ghost_pointer
    cur_distance

let compute_min_distance_to_ghosts our_pos ghost_states =
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 999999999
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let dist = dist our_pos ghost_pos
        if dist < cur_distance then
            cur_distance <- dist
        ghost_pointer <- List.tail ghost_pointer
    cur_distance


let eval_position maptree ghost_positions proposed_pos = // bigger is better!
    (compute_min_distance_to_ghosts proposed_pos ghost_positions) * 7
    //thresholded_distance map dims ghost_positions proposed_pos 0

let are_we_dead our_pos ghost_positions = 
    let mutable ghost_pointer = ghost_positions
    let mutable result = 0
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let dist = dist our_pos ghost_pos
        if dist = 0 then
            result <- 0-5000
        ghost_pointer <- List.tail ghost_pointer
    result // or -100000 if our_pos = one of the ghost_positions
        
let rec eq (x: obj) (y: obj): bool =
    let atomX = __isInt x
    let atomY = __isInt y
    if atomX && atomY then
        x = y
    else if atomX || atomY then
        __false
    else (eq (List.head x) (List.head y)) && (eq (List.tail x) (List.tail y))
     
let eval_path maptree ghost_status path = // returns eval_position of the final position + big penalty if we intersect with ghost
     let mutable path_pointer = reverse_list path
     let ghost_positions = extract_ghost_positions ghost_status
     let mutable result = eval_position maptree ghost_positions (List.head path)
     while not (__isInt path_pointer) do
        let cur_pos = List.head path_pointer // TODO: update ghost_position on each step
        result <- result + (are_we_dead cur_pos ghost_positions)
        path_pointer <- List.tail path_pointer
     result


let is_intersection maptree pos = 
    let mutable number_of_routes = 0
    let mutable i = 0
    let mutable exits = []
    let result = __false
    while i < 4 do
        let other_pos = move pos i
        if is_valid_move other_pos maptree then
            exits <- 1 :: exits
            number_of_routes <- number_of_routes + 1 
        else
            exits <- 0 :: exits
        i <- i + 1
    
    if number_of_routes = 2 then
        (getElement exits 0 = getElement exits 2) || (getElement exits 1 = getElement exits 3)
    else 
        number_of_routes > 2


let list_contains lst value = 
    let mutable result = __false
    let mutable lst_pointer = lst
    while not (__isInt lst_pointer) do
        if eq value (List.head lst_pointer) then
            result <- __true
        lst_pointer <- List.tail lst_pointer
    result
    

let predict_ghost_states_advanced maptree ghost_states =
   let mutable ghost_pointer = ghost_states
   let mutable result = []
   while not (__isInt ghost_pointer) do
        let cur_ghost = List.head ghost_pointer
        let ghost_vit = fst cur_ghost
        let ghost_pos = fst (snd cur_ghost)
        let ghost_move = snd (snd cur_ghost)
        if is_intersection maptree ghost_pos then
            let mutable i = 0
            while i < 4 do
                let ghost_new_pos = move ghost_pos i
                if is_valid_move ghost_new_pos maptree then
                    let new_ghost = (ghost_vit, (ghost_new_pos, ghost_move))
                    if not (list_contains result new_ghost) then
                        result <- new_ghost :: result
                i <- i + 1
        else // ghost has to move there!
            let ghost_new_pos = move ghost_pos ghost_move
            result <- (ghost_vit, (ghost_new_pos, ghost_move)) :: result
        ghost_pointer <- List.tail ghost_pointer
   result



let is_sane_move proposed_move maptree ghost_states = 
    let mutable result = __true
    if not (is_valid_move proposed_move maptree) then
        result <- __false
    else
        let ghost_posititions = extract_ghost_positions ghost_states
        let mutable ghost_pointer = ghost_posititions
        while not (__isInt ghost_pointer) do
            if eq proposed_move (List.head ghost_pointer) then
                result <- __false
            ghost_pointer <- List.tail ghost_pointer
    result

let rec do_find_best_path maptree ghost_states cur_path steps_left seen = 
     let mutable result = (0, [])
     if steps_left = 0 then
        result <- (eval_path maptree ghost_states cur_path, cur_path)
//        __print (888, fst result, reverse_list (snd result))
     else
        let latestPosition = List.head cur_path
        let newSeen = latestPosition :: seen
        let new_ghost_states = predict_ghost_states_advanced maptree ghost_states
        let awd = are_we_dead latestPosition (extract_ghost_positions new_ghost_states)
        if awd < 0 then
            result <- (awd, cur_path)
        else
            let mutable move_scores = []
            let mutable move_results = []
            let mutable i = 0
            move_results <- (cur_path) :: move_results
            move_scores <- ((eval_path maptree ghost_states cur_path) + steps_left) :: move_scores

            while i < 4 do
                let proposed_pos = move latestPosition i
                if not (is_sane_move proposed_pos maptree ghost_states) then
                    move_scores <- (0-5000) :: move_scores
                    move_results <- [] :: move_results
                else
                    let new_path = proposed_pos :: cur_path
                    let seen = []
                    let rec_res = (do_find_best_path maptree new_ghost_states new_path (steps_left - 1) seen)
                    move_results <- (snd rec_res) :: move_results
                    move_scores <- (fst rec_res) :: move_scores
                i <- i + 1
            let amm = getArgmaxAndMax (reverse_list move_scores)
            let new_path2 = getElement (reverse_list move_results) (fst amm)
            //__print (777, steps_left, snd amm, reverse_list new_path2)
            result <- (snd amm, new_path2)
     result

        
let find_best_move maptree current_pos ghost_status max_path_length =
    let mutable best_move = -1
    let mutable best_score = (0 - 1000000)
    let mutable best_path = []
    let mutable cur_path = []
    let mutable i = 0
    while i < 4 do
        let mutable next_pos = move current_pos i
        let mutable path_score = -100
        if is_valid_move next_pos maptree then            
            let her = do_find_best_path maptree ghost_status (next_pos::[]) (max_path_length - 1) []
            path_score <- fst her
            cur_path <- snd her
        if path_score > best_score then
            best_score <- path_score
            best_move <- i
            best_path <- reverse_list cur_path
        i <- i + 1
    __print (777, extract_ghost_positions ghost_status)
    i <- 0
    let mutable gs = ghost_status
    while i < max_path_length do
        gs <- predict_ghost_states_advanced maptree gs
        i <- i + 1

    __print (666, extract_ghost_positions gs)   
    __print (current_pos, best_move, best_score, best_path)
    best_move

        
//let find_move map dims cur_pos ghost_states prev_move =
//    let ghost_positions = predict_ghost_positions ghost_states
//    let score0 = eval_position map dims ghost_positions (move cur_pos 0)
//    let score1 = eval_position map dims ghost_positions (move cur_pos 1)
//    let score2 = eval_position map dims ghost_positions (move cur_pos 2)
//    let score3 = eval_position map dims ghost_positions (move cur_pos 3)  
//    let amm = getArgmaxAndMax [score0; score1; score2; score3]
//    let careful_result = fst amm
//    let farThreshold = (getCloseThreshold dims * 4) / 5
//    let mindist = compute_min_distance_to_ghosts cur_pos ghost_positions
////    __print (mindist, farThreshold)
//    if mindist > farThreshold then  // if we are safe, go eat something
//        let content0 = thresh_default (getMatrixElement map dims (move cur_pos 0)) 4 1
//        let content1 = thresh_default (getMatrixElement map dims (move cur_pos 1)) 4 1
//        let content2 = thresh_default (getMatrixElement map dims (move cur_pos 2)) 4 1
//        let content3 = thresh_default (getMatrixElement map dims (move cur_pos 3)) 4 1
//        let surroundings = [content0; content1; content2; content3]
////        __print surroundings
//        let content_amm = getArgmaxAndMax surroundings
//        let min_move = fst content_amm
//        if min_move = opposite prev_move then
//            let another_amm = getArgmaxAndMax_another_iDidntLikeThatOne surroundings min_move
//            fst another_amm
//        else min_move
//    else
//        careful_result


let doStep acc world =
    let maptree = fst acc
    let dims = snd acc
    let pacmanStatus : int * ((int*int) * (int * int)) = fst (snd world)
    let ghostsStatus : (int * ((int*int) * int)) list = fst (snd (snd world)) 
    let pacmanPosition : int * int  = fst (snd pacmanStatus) 
    let new_move = find_best_move maptree pacmanPosition ghostsStatus 2
    (acc, new_move)

let step acc world =    
    doStep acc world

let main world ghosts =    
    ((array2tree (fst world) 2, getMapDimensions (fst world)), step)
