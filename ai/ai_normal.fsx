﻿let __inline cmd = ()
let __isInt x = __inline "ATOM"; true
let __break = __inline "BRK"
let __print x = __inline "DBUG"
let __push x = ()
let __false = 0 > 1
let __true = 1 > 0

////////////////// STDLIB.LIST

let getArgmaxAndMax lst =
    if __isInt lst then
        __print 0xDEADBEEF
    let mutable max = List.head lst
    let mutable argmax = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur > max then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getArgmaxAndMax_another_iDidntLikeThatOne lst badIndex =
    let mutable max = 0
    let mutable argmax = badIndex
    let mutable i = 0
    let mutable tail = lst
    while not (__isInt tail) do
        let cur = List.head tail
        if (cur > max) || (cur = max && argmax = badIndex) then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getArgminAndMin lst =
    let mutable min = List.head lst
    let mutable argmin = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur < min then
            min <- cur
            argmin <- i
        tail <- List.tail tail
        i <- i + 1
    (argmin, min)

let getElement list index =
    let mutable list_pointer = list
    let mutable list_index = 0
    while list_index <> index do
        list_pointer <- List.tail list_pointer
        list_index <- list_index + 1
    List.head list_pointer

let getLength lst =
    let mutable res = 0
    let mutable l = lst
    while not (__isInt l) do
        l <- List.tail l
        res <- res + 1
    res

let getMapDimensions map =
    let height = getLength map
    let width = getLength (List.head map)
    (height, width)
    
let rec eq (x: obj) (y: obj): bool =
    let atomX = __isInt x
    let atomY = __isInt y
    if atomX && atomY then
        x = y
    else if atomX || atomY then
        0
    else (eq (List.head x) (List.head y)) && (eq (List.tail x) (List.tail y))

let kvpListContains list key =
    let mutable tail = list
    let mutable found = (0, null)
    while not (__isInt tail) do
        let cur = List.head tail
        if eq (fst cur) key then
            found <- (1, snd cur)
        tail <- List.tail tail
    found


////////////////// STDLIB.TREES

let treeSize tree = snd (snd (snd (snd tree)))
let treeRightChild tree = fst (snd (snd (snd tree)))
let treeLeftChild tree = fst (snd (snd tree))
let treeData tree = fst (snd tree)
let treeIsNode tree = fst tree = 1
let treeIsLeaf tree = fst tree = 0

let rec treeKth tree k =
    if treeIsLeaf tree then null
    else
        let leftChild = treeLeftChild tree
        let sizeLeft = treeSize leftChild
        if sizeLeft = k then treeData tree
        else if sizeLeft > k then treeKth leftChild k
        else treeKth (treeRightChild tree) (k - sizeLeft - 1)

let rec treePreorder tree tail =
    if treeIsLeaf tree then tail
    else 
        let cur = treeData tree
        let leftChild = treeLeftChild tree
        let rightChild = treeRightChild tree
        treePreorder leftChild (cur :: (treePreorder rightChild tail))

let rec treeUpdate tree k newValue =
    if treeIsLeaf tree then 
        __print 0xDEADBEEF
        tree
    else
//        __print (123, tree)
        let leftChild = treeLeftChild tree
        let sizeLeft = treeSize leftChild
        if sizeLeft = k then
            (1, (newValue, (leftChild, (treeRightChild tree, treeSize tree))))
        else if sizeLeft > k then
            (1, (treeData tree, ((treeUpdate leftChild k newValue), (treeRightChild tree, treeSize tree))))
        else
            (1, (treeData tree, (leftChild, ((treeUpdate (treeRightChild tree) (k - sizeLeft - 1) newValue), treeSize tree))))

let mapUpdate maptree coords newValue =
    let c = fst coords
    let r = snd coords
//    __print (122, coords, newValue)
    let row = treeKth maptree r
    let newRow = treeUpdate row c newValue    
    treeUpdate maptree r newRow

let rec array2tree lst level =
    if __isInt lst then
        (0, 0, 0, 0, 0)
    else
        let len = getLength lst
        let mid = len / 2
        let mutable left = []
        let mutable i = 0
        let mutable tail = lst
        while i < mid do
            left <- (List.head tail) :: left
            tail <- List.tail tail
            i <- i + 1
        let elem = List.head tail
        let key = if level = 1 then elem else array2tree elem (level - 1)
        tail <- List.tail tail
        left <- reverse_list left
        let leftTree = array2tree left level
        let rightTree = array2tree tail level
        (1, key, leftTree, rightTree, 1 + (treeSize leftTree) + (treeSize rightTree))

let rec getMapElement maptree coords =
    let c = fst coords
    let r = snd coords
    let row = treeKth maptree r
    treeKth row c

let extract_ghost_positions ghost_states =  // return a list of ghost positions [(x1, y1), (x2, y2), ...]
   let mutable ghost_pointer = ghost_states
   let mutable result = []
   while not (__isInt ghost_pointer) do
        let cur_ghost = List.head ghost_pointer
        let ghost_pos = fst (snd cur_ghost)
        result <- ghost_pos :: result

        ghost_pointer <- List.tail ghost_pointer
   result

////////////////// STDLIB.MATH

let abs x =
    if x > 0 then x else 0 - x

let dist pos1 pos2 = 
    abs (fst pos1 - fst pos2) + abs (snd pos1 - snd pos2)

let thresh x t = 
    if x > t then t else x

let thresh_default x t d =
    if x > t then d else x

let get_random_move acc = 
    let newAcc = (16525 * acc + 1014223) % 1296871
    (newAcc, newAcc % 4)

////////////////// STDLIB.MAP

let is_valid_move mv maptree =
    (getMapElement maptree mv) > 0

let move cur_pos dir = 
    let c = fst cur_pos
    let r = snd cur_pos
    let mutable result = (c-1, r) // LEFT
    if dir = 0 then // UP
        result <- (c, r-1)
    if dir = 1 then // RIGHT
        result <- (c+1, r)
    if dir = 2 then // DOWN
        result <- (c, r+1)
    result

let opposite move =
    (move + 2) % 4

let list_contains lst value = 
    let mutable result = __false
    let mutable lst_pointer = lst
    while not (__isInt lst_pointer) do
        if eq value (List.head lst_pointer) then
            result <- __true
        lst_pointer <- List.tail lst_pointer
    result
   
////////////////// OUR STRATEGY

let reverse_list list = 
   let mutable list_pointer = list
   let mutable result = []
   while not (__isInt list_pointer) do
        result <- (List.head list_pointer) :: result
        list_pointer <- List.tail list_pointer
   result

let getCloseThreshold dims = (if (fst dims < snd dims) then (fst dims) else (snd dims)) / 3

let min x y = if x < y then x else y

// our strategy: look for at all possible moves, maximize total distance to ghosts
let compute_sum_distances_to_ghosts dims our_pos ghost_states =
    let closeThreshold = getCloseThreshold dims
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 0
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let dist = dist our_pos ghost_pos
        cur_distance <- cur_distance + (thresh dist closeThreshold)
        ghost_pointer <- List.tail ghost_pointer
    cur_distance

let compute_min_distance_to_ghosts maptree our_pos ghost_states =
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 999999999
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let mutable dist = dist our_pos ghost_pos
//        if dist = 2 then
//            let min_diff = min (abs (fst our_pos - fst ghost_pos)) (abs (snd our_pos - snd ghost_pos))
//            if min_diff = 0 then
//                if fst our_pos = fst ghost_pos then
//                    let mid = (snd our_pos + snd ghost_pos) / 2
//                    if getMapElement maptree (fst our_pos, mid) = 0 then dist <- dist + 3
//                else
//                    let mid = (fst our_pos + fst ghost_pos) / 2
//                    if getMapElement maptree (mid, snd our_pos) = 0 then dist <- dist + 3
//            __print dist

        if dist < cur_distance then
            cur_distance <- dist
        ghost_pointer <- List.tail ghost_pointer
    cur_distance

//let thresholded_distance map dims ghost_states proposed_move defolt =
//    if is_valid_move proposed_move map dims then 
//        compute_sum_distances_to_ghosts dims proposed_move ghost_states 
//    else defolt


let eval_position maptree dims ghost_positions proposed_pos = // bigger is better!
    let t = getCloseThreshold dims
    thresh (compute_min_distance_to_ghosts maptree proposed_pos ghost_positions) t
    //thresholded_distance map dims ghost_positions proposed_pos 0

let predict_ghost_states_advanced maptree ghost_states our_pos =
   let mutable ghost_pointer = ghost_states
   let mutable result = []
   while not (__isInt ghost_pointer) do
        let cur_ghost = List.head ghost_pointer
        let ghost_vit = fst cur_ghost
        let ghost_pos = fst (snd cur_ghost)
        let ghost_move = snd (snd cur_ghost)
                
        let mutable moves = []
        let mutable i = 0
        while i < 4 do
            if (i <> (opposite ghost_move)) && is_valid_move (move ghost_pos i) maptree then
                moves <- i :: moves
            i <- i + 1
        if __isInt moves then
            moves <- (opposite ghost_move) :: []
        let cur_move =
            if __isInt (List.tail moves) then
                List.head moves
            else
                let mutable p = List.tail moves
                let mutable best_move = List.head moves
                let mutable best = dist (move ghost_pos best_move) our_pos
                while not (__isInt p) do
                    let cur = List.head p
                    let cur_manh = dist (move ghost_pos cur) our_pos
                    if cur_manh < best then
                        best <- cur_manh
                        best_move <- cur
                    p <- List.tail p
                best_move

        let ghost_new_pos = (move ghost_pos cur_move)
        let new_ghost = (ghost_vit, (ghost_new_pos, cur_move))
        result <- new_ghost :: result
//        let mutable p = moves
//        while not (__isInt p) do
//            let cur_move = List.head moves
//            let ghost_new_pos = (move ghost_pos cur_move)
//            let new_ghost = (ghost_vit, (ghost_new_pos, cur_move))
////                if not (list_contains result new_ghost) then
//            result <- new_ghost :: result
//            p <- List.tail p
        ghost_pointer <- List.tail ghost_pointer
//   __print (getLength ghost_states, getLength result)
   result

let is_sane_move proposed_move maptree ghost_states = 
    let mutable result = __true
    if not (is_valid_move proposed_move maptree) then
        result <- __false
    else
        let ghost_posititions = extract_ghost_positions ghost_states
        let mutable ghost_pointer = ghost_posititions
        while not (__isInt ghost_pointer) do
            if eq proposed_move (List.head ghost_pointer) then
                result <- __false
            ghost_pointer <- List.tail ghost_pointer
    result

let are_we_dead our_pos ghost_positions = 
    let mutable ghost_pointer = ghost_positions
    let mutable result = 0
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let dist = dist our_pos ghost_pos
        if dist = 0 then
            result <- 0-5000
        ghost_pointer <- List.tail ghost_pointer
    result // or -100000 if our_pos = one of the ghost_positions

let contentScore maptree cell fruit_score =
    let content = getMapElement maptree cell
    if content = 2 then 10
    else if content = 3 then 50
    else if content = 4 then fruit_score
    else 0
     
let eval_path maptree dims ghost_status path lambda fruit_score target = // returns eval_position of the final position + big penalty if we intersect with ghost
     let mutable path_pointer = reverse_list path
     let ghost_positions = extract_ghost_positions ghost_status
     let ghostRegularization = eval_position maptree dims ghost_positions (List.head path)
     let mutable result = lambda * ghostRegularization
     let mutable sum_of_eaten = 0
     while not (__isInt path_pointer) do
        let cur_pos = List.head path_pointer // TODO: update ghost_position on each step
        let cur_eaten = (contentScore maptree cur_pos fruit_score)
        sum_of_eaten <- sum_of_eaten + cur_eaten
        result <- result + (are_we_dead cur_pos ghost_positions) + cur_eaten
        path_pointer <- List.tail path_pointer
     if sum_of_eaten = 0 && ghostRegularization = getCloseThreshold dims && not (__isInt target) then
        result <- result - dist (List.head path) target
     result

let rec do_find_best_path maptree dims ghost_status cur_path steps_left seen_states lambda fruit_score target = 
     let mutable result = (0, [])
     let latestPosition = List.head cur_path     
     if steps_left = 0 then        
        let score = eval_path maptree dims ghost_status cur_path lambda fruit_score target
        let newSeen = ((latestPosition, steps_left), (score, cur_path)) :: seen_states
        result <- (score, cur_path, newSeen)
//        __print (888, fst result, reverse_list (fst (snd result)))
     else        
        let new_ghost_states = predict_ghost_states_advanced maptree ghost_status latestPosition
        let awd = are_we_dead latestPosition (extract_ghost_positions new_ghost_states)
        if awd < 0 then
            result <- (awd, cur_path, seen_states)
        else
            let mutable accSeen = seen_states
            let mutable move_scores = []
            let mutable move_results = []
            let mutable i = 0
            while i < 4 do
                let proposed_pos = move latestPosition i
                let resContains = kvpListContains accSeen (proposed_pos, (steps_left - 1))
                if not (is_sane_move proposed_pos maptree new_ghost_states) then
                    move_scores <- (0-5000) :: move_scores
                    move_results <- [] :: move_results
                else if fst resContains > 0 then
                    let cached = snd resContains
                    move_scores <- fst cached :: move_scores
                    move_results <- snd cached :: move_results
                else if list_contains cur_path proposed_pos then
                    // self-intersect, prune
                    let score = eval_path maptree dims ghost_status cur_path lambda fruit_score target
                    move_scores <- score :: move_scores
                    move_results <- cur_path :: move_results
                else
                    let new_path = proposed_pos :: cur_path
                    let rec_res = do_find_best_path maptree dims new_ghost_states new_path (steps_left - 1) accSeen lambda fruit_score target
                    accSeen <- snd (snd rec_res)
                    move_results <- (fst (snd rec_res)) :: move_results
                    move_scores <- (fst rec_res) :: move_scores
                i <- i + 1
            let amm = getArgmaxAndMax (reverse_list move_scores)
            let new_path2 = getElement (reverse_list move_results) (fst amm)
            //__print (777, steps_left, snd amm, reverse_list new_path2)
            accSeen <- ((latestPosition, steps_left), (snd amm, new_path2)) :: accSeen
            result <- (snd amm, new_path2, accSeen)
     result

let searchForGoods maptree our_pos =
    let mutable rows = treePreorder maptree []
    let mutable target = []
    let mutable r = 0
    while (not (__isInt rows)) && __isInt target do
        let mutable cells = treePreorder (List.head rows) []
        let mutable c = 0
        while __isInt target && (not (__isInt cells)) do
            if List.head cells > 1 && List.head cells < 5 then
                target <- (c, r)
            c <- c + 1
            cells <- List.tail cells
        r <- r + 1
        rows <- List.tail rows
    let mutable best = 99999999
    let mutable argbest = 0
    let mutable i = 0
    while i < 4 do
        let after = (move our_pos i)
        if is_valid_move after maptree then
            let cur = dist after target
            if cur < best then
                best <- cur
                argbest <- i
        i <- i + 1
    target, argbest
        
let find_best_move maptree dims current_pos ghost_status max_path_length lambda fruit_score target =
    let mutable new_target = target
    let mutable best_move = -1
    let mutable best_score = (0 - 1000000)
    let mutable best_path = []
    let mutable cur_path = []
    let mutable i = 0
    while i < 4 do
        let mutable next_pos = move current_pos i
        let mutable path_score = -100
        if is_valid_move next_pos maptree then            
            let her = do_find_best_path maptree dims ghost_status (next_pos::[]) (max_path_length - 1) [] lambda fruit_score target
            path_score <- fst her
            cur_path <- fst (snd her)
        if path_score > best_score then
            best_score <- path_score
            best_move <- i
            best_path <- reverse_list cur_path
        i <- i + 1
//    __print (predict_ghost_positions ghost_status)
//    __print (current_pos, best_move, best_score, best_path)
    if __isInt target then
        let mutable p = best_path
        let mutable eaten = 0
        while not (__isInt p) do
            let contentOnPath = getMapElement maptree (List.head p)
            if contentOnPath > 1 && contentOnPath < 5 then
                eaten <- eaten + 1
            p <- List.tail p
//        __print eaten
        if eaten = 0 then
            let search_res = searchForGoods maptree current_pos
//            __print search_res
            new_target <- fst search_res
            best_move <- snd search_res
            best_path <- (move current_pos best_move) :: []    
        
    let nextCell = List.head best_path
    let content = getMapElement maptree nextCell
    let mutable newMap = maptree
    if content > 1 && content < 5 then
        newMap <- mapUpdate maptree nextCell 1
    if eq nextCell target then
        new_target <- []
    ((newMap, dims, new_target), best_move)

let calcFruitScore dims =
    150 * ((fst dims * snd dims) / 100)

let doStep acc world =
    let maptree = fst acc
    let dims = fst (snd acc)
    let target = snd (snd acc)
    let fruit_score = calcFruitScore dims
    let pacmanStatus = fst (snd world) // : int * ((int*int) * (int * int))
    let ghostsStatus = fst (snd (snd world)) // : (int * ((int*int) * int)) list
    let pacmanPosition = fst (snd pacmanStatus) //  : int * int 
    let lookahead = if fst dims + snd dims > 200 then 2 else 6
    let lambda = 10 * lookahead / 3
    find_best_move maptree dims pacmanPosition ghostsStatus lookahead lambda fruit_score target

let step acc world =    
    doStep acc world

let main world ghosts =    
    ((array2tree (fst world) 2, getMapDimensions (fst world), []), step)
