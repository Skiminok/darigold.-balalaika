﻿let __inline cmd = ()
let __isInt x = __inline "ATOM"; true
let __break = __inline "BRK"
let __print x = __inline "DBUG"
let __push x = ()

////////////////// STDLIB.LIST

let getArgmaxAndMax lst =
    let mutable max = List.head lst
    let mutable argmax = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur > max then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getArgmaxAndMax_another_iDidntLikeThatOne lst badIndex =
    let mutable max = 0
    let mutable argmax = badIndex
    let mutable i = 0
    let mutable tail = lst
    while not (__isInt tail) do
        let cur = List.head tail
        if (cur > max) || (cur = max && argmax = badIndex) then
            max <- cur
            argmax <- i
        tail <- List.tail tail
        i <- i + 1
    (argmax, max)

let getArgminAndMin lst =
    let mutable min = List.head lst
    let mutable argmin = 0
    let mutable i = 1
    let mutable tail = List.tail lst
    while not (__isInt tail) do
        let cur = List.head tail
        if cur < min then
            min <- cur
            argmin <- i
        tail <- List.tail tail
        i <- i + 1
    (argmin, min)

let getElement list index =
    let mutable list_pointer = list
    let mutable list_index = 0
    while list_index <> index do
        list_pointer <- List.tail list_pointer
        list_index <- list_index + 1
    List.head list_pointer

let getLength lst =
    let mutable res = 0
    let mutable l = lst
    while not (__isInt l) do
        l <- List.tail l
        res <- res + 1
    res

let getMapDimensions map =
    let height = getLength map
    let width = getLength (List.head map)
    (height, width)

let getMatrixElementImpl matrix coordinates =
    let c = fst coordinates
    let r = snd coordinates
    let row = getElement matrix r
    let element = getElement row c
    element

let getMatrixElement matrix dims coordinates =
    let height = fst dims
    let width = snd dims
    let c = fst coordinates
    let r = snd coordinates
    let element = if c < 0 || c >= width || r < 0 || r >= height then -1 else getMatrixElementImpl matrix coordinates
    element

////////////////// STDLIB.MATH

let abs x =
    if x > 0 then x else 0 - x

let dist pos1 pos2 = 
    abs (fst pos1 - fst pos2) + abs (snd pos1 - snd pos2)

let thresh x t = 
    if x > t then t else x

let thresh_default x t d =
    if x > t then d else x

let get_random_move acc = 
    let newAcc = (16525 * acc + 1014223) % 1296871
    (newAcc, newAcc % 4)

////////////////// STDLIB.MAP

let is_valid_move mv map dims =
    (getMatrixElement map dims mv) > 0

let move cur_pos dir = 
    let c = fst cur_pos
    let r = snd cur_pos
    let mutable result = (c-1, r) // LEFT
    if dir = 0 then // UP
        result <- (c, r-1)
    if dir = 1 then // RIGHT
        result <- (c+1, r)
    if dir = 2 then // DOWN
        result <- (c, r+1)
    result

let opposite move =
    (move + 2) % 4
    // if move = 0 then 2 else if move = 1 then 3 else if move = 2 then 0 else 1
   
////////////////// OUR STRATEGY

let reverse_list list = 
   let mutable list_pointer = list
   let mutable result = []
   while not (__isInt list_pointer) do
        result <- (List.head list_pointer) :: result
        list_pointer <- List.tail list_pointer
   result

let predict_ghost_positions ghost_states =  // return a list of ghost positions [(x1, y1), (x2, y2), ...]
   let mutable ghost_pointer = ghost_states
   let mutable result = []
   while not (__isInt ghost_pointer) do
        let cur_ghost = List.head ghost_pointer
        let ghost_pos = fst (snd cur_ghost)
        result <- ghost_pos :: result

        let ghost_move =  snd (snd cur_ghost)
        let new_pos = move ghost_pos ghost_move
        result <- new_pos :: result

        let new_pos2 = move new_pos ghost_move
        result <- new_pos2 :: result

        ghost_pointer <- List.tail ghost_pointer
   
   reverse_list result


let getCloseThreshold dims = if (fst dims < snd dims) then (fst dims) / 2 else (snd dims) / 2

// our strategy: look for at all possible moves, maximize total distance to ghosts
let compute_sum_distances_to_ghosts dims our_pos ghost_states =
    let closeThreshold = getCloseThreshold dims
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 0
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let dist = dist our_pos ghost_pos
        cur_distance <- cur_distance + (thresh dist closeThreshold)
        ghost_pointer <- List.tail ghost_pointer
    cur_distance

let compute_min_distance_to_ghosts our_pos ghost_states =
    let mutable ghost_pointer = ghost_states
    let mutable cur_distance = 999999999
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let dist = dist our_pos ghost_pos
        if dist < cur_distance then
            cur_distance <- dist
        ghost_pointer <- List.tail ghost_pointer
    cur_distance

let thresholded_distance map dims ghost_states proposed_move defolt =
    if is_valid_move proposed_move map dims then 
        compute_sum_distances_to_ghosts dims proposed_move ghost_states 
    else defolt


let eval_position map dims ghost_positions proposed_pos = // bigger is better!
    compute_min_distance_to_ghosts proposed_pos ghost_positions
    //thresholded_distance map dims ghost_positions proposed_pos 0

let are_we_dead our_pos ghost_positions = 
    let mutable ghost_pointer = ghost_positions
    let mutable result = 0
    while not (__isInt ghost_pointer) do
        let ghost_pos = List.head ghost_pointer
        let dist = dist our_pos ghost_pos
        if dist = 0 then
            result <- 0-5000
        ghost_pointer <- List.tail ghost_pointer
    result // or -100000 if our_pos = one of the ghost_positions

     
let eval_path map dims ghost_status path = // returns eval_position of the final position + big penalty if we intersect with ghost
     let mutable path_pointer = reverse_list path
     let ghost_positions = predict_ghost_positions ghost_status
     let mutable result = eval_position map dims ghost_positions (List.head path)
     while not (__isInt path_pointer) do
        let cur_pos = List.head path_pointer // TODO: update ghost_position on each step
        result <- result + are_we_dead cur_pos ghost_positions
        path_pointer <- List.tail path_pointer
     result



let rec do_find_best_path map dims ghost_status cur_path steps_left = 
     let mutable result = (0, [])
     if steps_left = 0 then
        result <- (eval_path map dims ghost_status cur_path, cur_path) // TODO: tweak eval_path
        __print (888, fst result, reverse_list (snd result))
     else
        let mutable move_scores = []
        let mutable move_results = []
        let mutable i = 0
        while i < 4 do
            let proposed_pos = move (List.head cur_path) i
            if not (is_valid_move proposed_pos map dims) then
                move_scores <- (0-5000) :: move_scores
                move_results <- [] :: move_results
            else
                let new_path = proposed_pos :: cur_path
                let rec_res = (do_find_best_path map dims ghost_status new_path (steps_left - 1))
                move_results <- (snd rec_res) :: move_results
                move_scores <- (fst rec_res) :: move_scores
            i <- i + 1
        let amm = getArgmaxAndMax (reverse_list move_scores)
        let new_path2 = getElement (reverse_list move_results) (fst amm)
        //__print (777, steps_left, snd amm, reverse_list new_path2)
        result <- (snd amm, new_path2)
     result

        
let find_best_move map dims current_pos ghost_status max_path_length =
    let mutable best_move = -1
    let mutable best_score = (0 - 1000000)
    let mutable best_path = []
    let mutable cur_path = []
    let mutable i = 0
    while i < 4 do
        let mutable next_pos = move current_pos i
        let mutable path_score = -100
        if is_valid_move next_pos map dims then
            let her = do_find_best_path map dims ghost_status (next_pos::[]) (max_path_length - 1)
            path_score <- fst her
            cur_path <- snd her
        if path_score > best_score then
            best_score <- path_score
            best_move <- i
            best_path <- reverse_list cur_path
        i <- i + 1
    __print (predict_ghost_positions ghost_status)
    __print (current_pos, best_move, best_score, best_path)
    best_move

        
let find_move map dims cur_pos ghost_states prev_move =
    let ghost_positions = predict_ghost_positions ghost_states
    let score0 = eval_position map dims ghost_positions (move cur_pos 0)
    let score1 = eval_position map dims ghost_positions (move cur_pos 1)
    let score2 = eval_position map dims ghost_positions (move cur_pos 2)
    let score3 = eval_position map dims ghost_positions (move cur_pos 3)  
    let amm = getArgmaxAndMax [score0; score1; score2; score3]
    let careful_result = fst amm
    let farThreshold = (getCloseThreshold dims * 4) / 5
    let mindist = compute_min_distance_to_ghosts cur_pos ghost_positions
//    __print (mindist, farThreshold)
    if mindist > farThreshold then  // if we are safe, go eat something
        let content0 = thresh_default (getMatrixElement map dims (move cur_pos 0)) 4 1
        let content1 = thresh_default (getMatrixElement map dims (move cur_pos 1)) 4 1
        let content2 = thresh_default (getMatrixElement map dims (move cur_pos 2)) 4 1
        let content3 = thresh_default (getMatrixElement map dims (move cur_pos 3)) 4 1
        let surroundings = [content0; content1; content2; content3]
//        __print surroundings
        let content_amm = getArgmaxAndMax surroundings
        let min_move = fst content_amm
        if min_move = opposite prev_move then
            let another_amm = getArgmaxAndMax_another_iDidntLikeThatOne surroundings min_move
            fst another_amm
        else min_move
    else
        careful_result

let doStep acc world =
    let prev_move = acc
    let map = fst world
    let dims = getMapDimensions map
    let pacmanStatus = fst (snd world) // : int * ((int*int) * (int * int))
    let ghostsStatus = fst (snd (snd world)) // : (int * ((int*int) * int)) list
    let pacmanPosition = fst (snd pacmanStatus) //  : int * int 
    //let new_move = find_move map dims pacmanPosition ghostsStatus prev_move
    let new_move = find_best_move map dims pacmanPosition ghostsStatus 4
    (new_move, new_move)

let step acc world =    
    doStep acc world

let main world ghosts =    
    (0, step)
