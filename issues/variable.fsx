let getPacmanPosition world =
    let pacmanStatus = fst (snd world)
    let position = fst (snd pacmanStatus)
    position

//let getMatrixCell matrix position =
//    printfn "%A" position
//    let mutable matrix_pointer = matrix
//    let mutable row_index = 0
//    while row_index <> fst position do
//        matrix_pointer <- snd matrix_pointer
//        row_index <- row_index + 1
//    done
//    matrix_pointer <- fst matrix_pointer
//    let mutable col_index = 0
//    while col_index <> snd position do
//       matrix_pointer <- snd matrix_pointer
//        col_index <- col_index + 1
//    done
//    fst matrix_pointer

let doStep acc world =
    let UP = 0
    let RIGHT = 1
    let DOWN = 2
    let LEFT = 3
    let map = fst world
    let pacmanStatus = fst (snd world)
    let ghostsStatus = fst (snd (snd world))
    let fruitStatus = snd (snd (snd world))
    let pacmanPosition = getPacmanPosition world
    // printfn "%A" pacmanPosition
    // let nextPosition = getMatrixCell map (fst pacmanPosition, snd (pacmanPosition) - 1)
    let move = LEFT //if nextPosition = 0 then 0 else 3
    (1, move)

let step acc world =
    doStep acc world

let main world ghosts =
    (1, step)
